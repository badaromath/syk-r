######################## CARREGANDO PACOTES  ############################
#########################################################################
#########################################################################
# instalando
install.packages("showtext")


install.packages("/home/user/Downloads/RACCOON/R.accoon_0.1.9.tar.gz", 
                 repos = NULL, 
                 type="source")

??showtext





######################## IMPORTAÇÃO DE DADOS ############################
#########################################################################
#########################################################################

library(tidyverse)
###### ARQUIVO .txt

df = read.table("df.txt", header = TRUE, sep = ";")


###### DATAFRAME WEB

dfweb = read.table(
  "https://archive.ics.uci.edu/ml/machine-learning-databases/auto-mpg/auto-mpg.data", 
  sep = "", 
  header=F)


colnames(dfweb) = c("mpg", "cylinders", "displacement", "horsepower", "weight",     ##renomeando colunas
                    "acceleration", "model_year", "origin", "car_name")

head(dfweb)                                                                         ##cabeçalhodos dos dados



###### ARQUIVO .csv

dfcsv = read.csv("df.csv", header = TRUE, sep = ";")


###### ARQUIVO .xlsx

library(readxl)
dfxl = read_xlsx("df.xlsx")


##### TABELAS EM HTML

library(htmltab)
pais = htmltab("https://pt.wikipedia.org/wiki/Lista_de_pa%C3%ADses_por_popula%C3%A7%C3%A3o", 
               which = 1)


###### Google Sheets

library(googlesheets4)

dfsheet = read_sheet(
  "https://docs.google.com/spreadsheets/d/16kYm6dwGvciSPCndZ3myHV07SHObcjRbKLrzrlPVl7k/edit#gid=1059526778",
  col_names = TRUE, 
  sheet = 'Dados')




###### Google Big Query
####################################################
library(bigrquery)

######### EXECUTA A QUERY E ARMAZENA A VIEW

# Setando o nome do projeto
projectid = "raccoon-bi"

# Query a ser executada
sql_face <- "SELECT * 
        FROM `Mosaic.facebook_nutricaodesafras_posts`"

sql_insta <- "SELECT * 
        FROM `Mosaic.instagram_nutricaodesafras_media`"

# performando a consulta com bigrquery
tb_face <- bq_project_query(projectid, sql_face)

tb_insta <- bq_project_query(projectid, sql_insta)

# baixando os dados
post_face = bq_table_download(tb_face)
post_insta = bq_table_download(tb_insta)

head(post_insta)

########################### COMANDOS UTEIS ##############################
#########################################################################
#########################################################################

#### Lista
conc = c(1,2,3,4,5)

#### Matrix

mat = matrix(c(conc,-conc), 
             nrow = 5, 
             ncol = 2, 
             byrow = F)


#### data frame:

dfmat = as.data.frame(mat)

dfmat = tibble(mat)

dfmat = tibble(x = conc, y = -conc)


#### Acessando valores do dataframe:

linha1 = dfmat[1,]                          
coluna1 = dfmat[,1]                         

#### média
media = mean(conc)


#### desvio padrão
desvio = sd(conc)


#### variancia
variancia = var(conc)


#### tamanho
tamanho = length(conc)


#### somatorio
soma = sum(conc)


#### FOR

mx = matrix(NA, nrow=10, ncol=2)
for (i in 1:10) {
  mx[i,1] = i
  mx[i,2] = 7*i
}


#### IF

pares = matrix(NA,ncol = 2, nrow = 10)
for (j in 1:10) {
  if(mx[j,2]%%2 == 0){
    pares[j,1] = mx[j,1]
    pares[j,2] = mx[j,2]
  }
  else{
    pares[j,1] = "Ímpar"
    pares[j,2] = "Ímpar"
  }
}

#### WHILE

a = 1
while (a <= 10){
  print(a*2)
  a = a+1
}


#### WHICH

# Localiza a posição quando condição satisfeita
which(pares[,1] != "Ímpar")

# Mostrando os valores quando condição satisfeita
pares[which(pares[,1] != "Ímpar"),2]


#### FUNÇÃO

soma = function(vetor){
  sum(vetor)
}

med = function(vetor){
  mean(vetor)
}



############################# Manipulacao ###############################
#########################################################################
#########################################################################


library(dplyr)

######################################## SELECT:

head(dfweb)
motor = select(dfweb, cylinders:horsepower)
head(motor)

######################################## FILTER:

#carros produzidos em 1975:
car_1975 = filter(dfweb, model_year == 75)

head(car_1975)

#carros produzidos nos EUA em 1973:
car_1973_EUA = filter(dfweb, model_year == 73 & origin == 1)

head(car_1973_EUA)


######################################## ARRANGE:
dfweb = arrange(dfweb, model_year)

head(dfweb)


######################################## RENAME:
head(dfweb)

dfweb = rename(dfweb, model_name = car_name, hp = horsepower)

head(dfweb)

######################################## MUTATE:

dfweb = mutate(dfweb, weight = weight*0.453592)
head(dfweb)

dfweb = mutate(dfweb, displacement = displacement*16.3871)
head(dfweb)

head(arrange(dfweb, -displacement))


######################################## IF-ELSE PARA DADOS DICOTOMICOS:
exemplo = data.frame(
  cbind(
    c(1,2,3,4,5,6),
    c("impar","par","impar","par","impar","par")
    )
  )
                                
exemplo$X2 = ifelse(exemplo$X2 == "par", 0 , 1)


######################################## GROUP BY:
model_years = group_by(dfweb, model_year)

head(model_years)

summarize(model_years, hp = mean(hp, na.rm = TRUE), 
          deslocamento = mean(displacement, na.rm = TRUE), 
          peso = mean(weight, na.rm = TRUE))

######################################## Interceções entre 2 conjuntos

## 
both <- post_insta$caption %in% post_face$message 

length(
  post_insta$caption
    [
    which(both == 'TRUE')                   # WHERE both = 'TRUE'
    ]
  )






############################# GGPLOT2 ###################################
#########################################################################
#########################################################################
library(dplyr)
library(ggplot2)
library(R.accoon)

#### data IRIS
data("iris")

### Pacotes R.accoon
inst.pacotes("color_raccoon")
inst.pacotes("theme_raccoon")


### Group BY espécies
iris$Species = as.factor(iris$Species)

### Chamando ggplot para os dados:
g = ggplot(data = iris)
dev.new()

### Definindo estéticas do gráfico:
g + geom_bar(aes(x = Species, y = Sepal.Length, fill = Species), 
             stat="identity", position = "dodge") + 
  scale_fill_manual(values = color_raccoon()) +               #Cores Raccoon
  theme_raccoon()                                             # Tema Raccoon


### DICA: Usar indentation para melhorar disposição do código

# Pon
g + geom_point(aes(
                x = Species, 
                y = Sepal.Length, 
                fill=Species, 
                color = Species))+ 
  scale_color_manual(values = color_raccoon()) +                          
  theme_raccoon() 

g + geom_boxplot(aes(
                x = Species, 
                y = Sepal.Length, 
                fill=Species))+ 
  scale_fill_manual(values = color_raccoon()) +                          
  theme_raccoon() 

g + geom_histogram(aes(
                x = Sepal.Length), 
                color = "black", 
                fill = "#F1B579")+
  theme_raccoon() 

g + geom_histogram(aes(
                x = Petal.Length, 
                fill = Species), color = "black")+ 
  scale_fill_manual(values = color_raccoon()) +  
  scale_color_manual(values = color_raccoon())+
  theme_raccoon() 





########################### FANCY GRAPHS ################################
#########################################################################
#########################################################################

library(dygraphs)
library(tidyverse)
library(lubridate)
library(xts)         
library(BatchGetSymbols)
library(dplyr)
library(plotly)
library(hrbrthemes)
library(gganimate)
library(babynames)
library(gapminder)
library(gifski)

#### Importando a ação da Petrobrás PETR4.SA
açoes  =  c ( 'PETR4.SA' )                                    #### vetor com as ações pretendidas
data.inicial  =  Sys.Date () -500                             #### data atual - 500 dias
data.final  =  Sys.Date ()                                    #### data atual
açao =  BatchGetSymbols (tickers  =  açoes ,                  #### puxando os dados do yahoo finance
                         first.date  =  data.inicial , 
                         last.date  =  data.final ,  
                         do.cache = FALSE )

#### DATAFRAME da série
serie = açao$df.tickers

#### GGPLOTLY
g <- serie %>%
  ggplot(aes(x=ref.date, y=price.open)) +
  geom_area(fill="#69b3a2", alpha=0.5) +
  geom_line(color="#69b3a2") +
  ylab("Open Price PETR4.SA (R$)")+
  xlab("Data")+
  scale_fill_manual(values = color_raccoon()) +                          
  theme_raccoon() 

#### Tornar interativo com o ggplotly
g1 <- ggplotly(g); g1

price_open <- xts(x = serie$price.open, order.by = serie$ref.date)
colnames(price_open) = "Preço R$"

#### DYGRAPH
g2 <- dygraph(price_open) %>%
  dyOptions(labelsUTC = TRUE, fillGraph=TRUE, fillAlpha=0.1, drawGrid = FALSE, colors="#D8AE5A") %>%
  dyRangeSelector() %>%
  dyCrosshair(direction = "vertical") %>%
  dyHighlight(highlightCircleSize = 5, highlightSeriesBackgroundAlpha = 0.2, hideOnMouseOut = FALSE)  %>%
  dyRoller(rollPeriod = 1); g2


#### GGANIMATE


açoes2  =  c ('PETR4.SA', 'VALE3.SA')                          #### vetor com as ações pretendidas  
data.inicial  =  Sys.Date () -365                              #### data atual - 365 dias
data.final  =  Sys.Date ()                                     #### data atual
açao2 =  BatchGetSymbols (tickers  =  açoes2 ,                 #### puxando os dados do yahoo finance
                          first.date  =  data.inicial , 
                          last.date  =  data.final ,  
                          do.cache = FALSE )

serie2 = açao2$df.tickers


#### Filtrando a série (GROUP BY)
head(serie2)
don <- serie2 %>% 
  filter(ticker %in% c("PETR4.SA", "VALE3.SA"))

don$ref.date = as.Date(don$ref.date)
#### GGANIMATE


gganim = ggplot(data = don, aes(x=ref.date, y=price.open, group=ticker, color=ticker)) +
  geom_line(size=2) +
  geom_point(size = 2) +
  scale_colour_viridis_d() +
  ggtitle("Ações da PETR4 e VALE3") +
  scale_fill_manual(values = color_raccoon()) +                          
  theme_raccoon() +
  ylab("Preço R$") +
  xlab("Data (último ano)")+
  transition_reveal(ref.date)+
  theme(text = element_text(size = 20))

animate(gganim, duration = 10, fps = 20, width = 1000, height = 500, renderer = gifski_renderer("PETR4_VALE3_500_days.gif"))
anim_save("/home/user/Área de Trabalho/OKR ds/PETR4_VALE3_500_days.gif")




